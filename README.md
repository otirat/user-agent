# User Agent

It is the orchestrator of the different action we need to do for the 22.11 demo.

## Getting started

### IDs

#### Participants

All participants IDs are generated according to the following rule
```python
# Participants
self.num_id = hashlib.sha256(bytes(self.get_designation()+self.get_registration_number()+self.get_hqa_country(),'utf-8')).hexdigest()
# Location
Designation
# ServiceOffering
Designation
# LocatedServiceOffering

```
## Bootstrap the provider

### OVHCLoud

```sh
curl --location --request POST 'http://localhost:5001/api/bootstrap-provider?provider_did=did:web:ovh.gaia-x.community/public/json-ld/provider.json&provider_base_did=did:web:ovh.gaia-x.community' \
--form 'location=@"/Users/steyssier/GIT/gaia/gxfs/user-agent/csv-files/3-Locations-Table 1.csv"' \
--form 'service=@"/Users/steyssier/GIT/gaia/gxfs/user-agent/csv-files/4-Services-Table 1.csv"'
```

### ABC Federation

```sh
curl --location --request POST 'http://localhost:5001/api/bootstrap-provider?provider_did=did:web:abc-federation.gaia-x.community/public/json-ld/provider.json&provider_base_did=did:web:abc-federation.gaia-x.community' \
--form 'compliance_ref=@"/Users/steyssier/GIT/gaia/gxfs/user-agent/csv-files/1-ComplianceResources-Table 1.csv"' 
```


### Execution branch pb

```sh
# TLS_CERT_PATH à ne pas définir si pas de cert
cat .env
TLS_CERT_PATH=/Users/steyssier/GIT/gaia/abc-tls.crt
SERVER_NAME=abc-federation.gaia-x.community
PARTICIPANT_NAME=abc-federation
PARENT_DOMAIN=gaia-x.community% 
# commande
curl --location --request POST 'http://localhost:5001/api/bootstrap-provider' \
--form 'compliance_ref=@"/Users/steyssier/GIT/gaia/gxfs/data-initializer/code/data-compliance/ABC-FEDERATION/ComplianceResource.csv"

# Le fichier ComplianceResource.csv est modifié
#
ID;Champ1;Compliance Resource Id;CR-ID;Ref Designation;Extrait;Category;Valid from;Valid Until;Reference Doc;Assertion Method;Assertion Date;Issuer Designation;
1;1;CResid-XXX001;CR-01;CISPE Data Protection CoC;09/02/2021;Data Protection;2/2/22;;https://www.codeofconduct.cloud/wp-content/uploads/2022/01/CISPE-Cloud-Data-Protection-Code-of-Conduct-DIGITAL.pdf;Certified;;;
2;2;CResid-XXX002;CR-02;SWIPO IaaS CoC;2020 v 3.0;Data Porting;5/10/21;;;Self-Declared;;;
3;3;CResid-XXX003;CR-03;HDS-FR;1.1 finale;Security;6/30/22;1/13/25;https://esante.gouv.fr/sites/default/files/media_entity/documents/hds_referentiel_de_certification_v1.1f_mai2018.pdf;Certified;;;

```