from catalog_from_csv import utils


def test_answer():
    result = utils.extract_filename_of_did("did:web:example.org/data.json")

    assert result == "data.json"

