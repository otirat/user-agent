import Config
from catalog_from_csv.service_generator import ServiceGenerator


def test_build_service_types():
    generator = ServiceGenerator("../template/tpl-service-offering.json",
                                 "../template/tpl-located-service-offering.json",
                                 "",
                                 Config.DIRECTORY_FOR_SERVICE_OFFERING,
                                 Config.DIRECTORY_FOR_LOCATED_SERVICE_OFFERING,
                                 "",
                                 "../public")

    service_type = generator.build_service_type("Backup;Datastorage")

    print(service_type)
    # assert service_type == "[{'@value': 'Backup', '@type': 'xsd:string'},
    # {'@value': 'Datastorage', '@type': 'xsd:string'}]"
