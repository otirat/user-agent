import json
import logging
import os
import traceback
from flask import Flask, send_from_directory, request
from flask_autoindex import AutoIndex
from werkzeug.utils import secure_filename

import Config
from catalog_from_csv import utils
from catalog_from_csv.catalog_generator import CatalogGenerator

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = Config.PUBLIC_FOLDER_TO_EXPOSE
app.config['TMP_FOLDER'] = Config.TMP_FOLDER  # Directory to store temporary files like csv

ALLOWED_EXTENSIONS = {'csv', 'json'}

AutoIndex(app, browse_root=os.path.join(os.path.curdir, app.config['UPLOAD_FOLDER']))
@app.route('/healthcheck')
def healthcheck():
    return f"User Agent for {Config.PARTICIPANT_NAME} OK"


# https://registry.gaia-x.eu/v2206/api/shape/files?file=service-offering&type=jsonld
# https://registry.gaia-x.eu/v2206/api/shape/files?file=service-offering&type=jsonld
@app.route('/api/shape/files')
def download_shape():
    args = request.args
    entity_type = args.get("entity_type")
    entity_format = args.get("format")
    print(entity_type)
    print(entity_format)

    return "OK for shape"


@app.route('/<path:filename>')
def expose_directory(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename, as_attachment=False)


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/api/test', methods=['POST'])
def test():
    args = request.args
    provider_did = args.get("provider_did")
    print(provider_did)

    return f"Hello {provider_did}"


@app.route('/clean-data/', methods=['DELETE'])
def clean_data():
    utils.clean_output_directories([
        Config.DIRECTORY_FOR_LOCATED_SERVICE_OFFERING,
        Config.DIRECTORY_FOR_SERVICE_OFFERING,
        Config.DIRECTORY_FOR_LOCATION,
        Config.DIRECTORY_FOR_THIRD_PARTY_COMPLIANCE_CERT_CRED,
        Config.OUT_FOLDER
    ])

    return "DATA CLEANED"

@app.route('/api/vc-request', methods=['PUT'])
def vc_request_participant():
    logging.info(f"Request VC participant {request.json}")

    self_description = request.json


    # TODO: Check SHACL validity
    # Check object is known
    # Check ID match current PARTICIPANT
    
    from clients.vc_issuer_client import put_json_ld_content
    response = put_json_ld_content(self_description)

    logging.info(f"Response from VC issuer {response.status_code}")
    signed_vc = json.loads(response.content)
    
    id = signed_vc["@id"]
    path = id.replace(f"https://{Config.SERVER_NAME}", "")
    utils.save_on_disk_by_filepath(f"{Config.PUBLIC_FOLDER_TO_EXPOSE}/{path}", signed_vc)
    
    # Store it inside the catalog
    
    return signed_vc
    
@app.route('/api/store_credential/<string:cred_type>', methods=['POST'])
def store_credential(cred_type):
    logging.info(f"Try to store a new credential of type {cred_type}")
    if cred_type not in Config.ALLOWED_CRED_TYPES:
        logging.error(f"{cred_type} not allowed. Types allowed: {', '.join(Config.ALLOWED_CRED_TYPES)}")
        return "TYPE NOT ALLOWED"
    
    FORM_FILE = "credential"
    if FORM_FILE in request.files and allowed_file(request.files[FORM_FILE].filename):
        file = request.files[FORM_FILE]

        try:
            js = json.loads(file.read())
            object_hash = js.get("credentialSubject").get("@id").split('/')[-1]
        except:
            return "ERROR"
        
        output_participant_directory = os.path.join(Config.PARTICIPANT_FOLDER, Config.PARTICIPANT_HASH)
        os.path.isdir(output_participant_directory) or os.makedirs(output_participant_directory)

        output_cred_type_directory = os.path.join(output_participant_directory, cred_type, object_hash)
        os.path.isdir(output_cred_type_directory) or os.makedirs(output_cred_type_directory)
        
        output_filepath = os.path.join(output_cred_type_directory, "data.json")
        utils.save_on_disk_by_filepath(output_filepath, js)

        return "OK"
    else:
        return "ERROR"
    
@app.route('/api/bootstrap-provider', methods=['POST'])
def bootstrap_provider():
    logging.info(f"Start bootstrap for provider {Config.PARTICIPANT_NAME}.")

    my_catalog_generator = CatalogGenerator()

    location_filename = retrieve_and_store_filepath('location')
    service_filename = retrieve_and_store_filepath('service')
    compliance_ref_filename = retrieve_and_store_filepath('compliance_ref')
    layers_filename = retrieve_and_store_filepath('layers')
    keywords_filename = retrieve_and_store_filepath('keywords')

    try:
        my_catalog_generator.generate_catalog(location_filename, service_filename, compliance_ref_filename, layers_filename, keywords_filename)
    except Exception as e:
        logging.error(f"Unable to generate catalog because of {e}")
        logging.error(f"Cause: {traceback.format_exc()}")

        return "ERROR"

    return "OK"


def retrieve_and_store_filepath(name):
    filepath = None

    if name in request.files and allowed_file(request.files[name].filename):
        logging.info(f"{name} file received, store it.")

        filename = secure_filename(request.files[name].filename)
        request.files[name].save(os.path.join(app.config['TMP_FOLDER'], filename))
        filepath = os.path.join(app.config['TMP_FOLDER'], filename)
    else:
        logging.info(f"No {name} file in {request.files}")

    return filepath


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=Config.API_PORT_EXPOSED, debug=True)
