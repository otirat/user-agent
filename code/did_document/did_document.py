from jwcrypto import jwk

import logging
import ssl
import json
import os
from cert_chain_resolver.api import resolve
from catalog_from_csv import utils

class DIDDocumentGenerator:

  cert_chain_filename = "chain.pem"
    
  def __init__(self, tls_cert_path: str, out_folder: str, server_name: str, participant_hash: str):
        self.tls_cert_path = tls_cert_path
        self.out_folder = out_folder
        self.server_name = server_name
        self.participant_hash = participant_hash
        self.out_cert_chain_path = self.out_folder+ "/.well-known/"+self.cert_chain_filename
        
  def _retrieve_cert_chain(self):
    with open(self.tls_cert_path, 'rb') as fin:
      fb = fin.read()
      chain = resolve(fb)  
      import os
      os.path.isdir(self.out_folder+ "/.well-known/") or os.mkdir(self.out_folder+ "/.well-known/")
      logging.info("Write cert chain to %s"%(self.out_cert_chain_path))
      with open(self.out_cert_chain_path, 'wt') as fout:
        for cert in chain:
          fout.write(cert.export())
          
  def _create_did_document(self):
    out_did_document_path = self.out_folder+ "/.well-known/did.json"
    verification_method = 'did:web:'+self.server_name
    # Fetch certificate
    logging.info("Retrieve cert for %s"%(self.server_name))
    #cert = ssl.get_server_certificate(addr=(self.server_name, 443), ca_certs=self.out_cert_chain_path)
    cert = ssl.get_server_certificate(addr=(self.server_name, 443))
    # Build JWK in RFC 7797 format
    key = jwk.JWK.from_pem(bytes(cert, 'UTF-8'))
    publicKeyJwk = key.export(as_dict=True)
    publicKeyJwk['alg'] = 'PS256'
    publicKeyJwk['x5u'] = 'https://'+self.server_name+'/.well-known/'+self.cert_chain_filename
    # Build DID document for the bare domain
    did = {
      '@context': ['https://www.w3.org/ns/did/v1'],
      'id': verification_method,
      'verificationMethod': [
        {
          '@context': 'https://w3c-ccg.github.io/lds-jws2020/contexts/v1/',
          'id': verification_method,
          'type': "JsonWebKey2020",
          'controller': verification_method+'#JWK2020-RSA',
          'publicKeyJwk': publicKeyJwk
        }
      ],
      'assertionMethod': [verification_method+ '#JWK2020-RSA']
    }
    utils.save_on_disk_by_filepath(filepath = out_did_document_path, data = did)
      
    # Generate did document for participant:hash path
    out_did_document_path = self.out_folder+ (f"/participant/{self.participant_hash}/did.json")
    did['id'] = did['id']+f":participant:{self.participant_hash}"
    utils.save_on_disk_by_filepath(filepath = out_did_document_path, data = did)
    
         
  def create_files(self):
    self._retrieve_cert_chain()
    self._create_did_document()

  def get_did(self):
    return f"did:web:{self.server_name}/.well-known/did.json"