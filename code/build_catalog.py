"""
Script permettant de construire un fichier "generated_catalog.json" pouvant être envoyé au catalog pour l'alimenter.

Il va en fait reprendre les différents fichiers du répertoire public/json-ld pour les concaténer dans un objet graph
"""
import json
from os import listdir
from os.path import isdir, isfile, join
import logging
import copy

catalog_tpl = {
    "@context": {
        "cred": "https://www.w3.org/2018/credentials/#",
        "gax-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#",
        "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
        "xsd": "http://www.w3.org/2001/XMLSchema#",
        "gax-validation": "https://schemas.abc-federation.gaia-x.community/wip/vocab/validation#",
        "vcard": "http://www.w3.org/2006/vcard/ns#",
        "gax-core": "https://schemas.abc-federation.gaia-x.community/wip/vocab/core#",
        "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#",
        "dct": "http://purl.org/dc/terms/",
        "sh": "http://www.w3.org/ns/shacl#",
        "gax-node": "https://schemas.abc-federation.gaia-x.community/wip/vocab/node#",
        "dcat": "http://www.w3.org/ns/dcat#",
        "gax-resource": "https://schemas.abc-federation.gaia-x.community/wip/vocab/resource#"
    },
    "@graph": []
}


def browse_file(graph, directory):
    for f in listdir(directory):
        if isdir(join(directory, f)):
            browse_file(graph, join(directory, f))
        elif isfile(join(directory, f)):
            json_ld = json.load(open(join(directory, f)))
            graph.append(json_ld)
        else:
            print("Unknown type")


def launch_generation_from_directory(directory, output_filepath):
    logging.info("BEGIN CATALOG BUILD from %s to %s", directory, output_filepath)
    catalog = copy.deepcopy(catalog_tpl)

    browse_file(catalog['@graph'], directory)

    result_file = open(output_filepath, "w", encoding='utf8')
    result_file.write(json.dumps(catalog))
    result_file.close()

    logging.info("END OF GENERATION")


def launch_generation_from_file(file, output_filepath):
    logging.info("BEGIN CATALOG BUILD from %s to %s", file, output_filepath)
    catalog = copy.deepcopy(catalog_tpl)

    json_ld = json.load(open(file, "r"))
    catalog['@graph'].append(json_ld)

    result_file = open(output_filepath, "w", encoding='utf8')
    result_file.write(json.dumps(catalog))
    result_file.close()

    logging.info("END OF GENERATION")


if __name__ == "__main__":
    # launch_generation_from_directory("./public/json-ld", "./generated_catalog.json")
    launch_generation_from_directory("./out", "./tmp/generated_catalog.json")
