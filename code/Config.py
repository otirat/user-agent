import os
import logging
import hashlib
from dotenv import load_dotenv

load_dotenv()

PARTICIPANT_NAME = os.getenv('PARTICIPANT_NAME', 'ovhcloud')
API_PORT_EXPOSED = os.getenv('API_PORT_EXPOSED', 5001)

TEMPLATE_LOCATION_FILEPATH = os.getenv('TEMPLATE_LOCATION_FILEPATH', "./template/tpl-location.json")
TEMPLATE_SERVICE_OFFERING_FILEPATH = os.getenv('TEMPLATE_SERVICE_OFFERING_FILEPATH',
                                               "./template/tpl-service-offering.json")
TEMPLATE_LOCATED_SERVICE_OFFERING_FILEPATH = os.getenv('TEMPLATE_LOCATED_SERVICE_OFFERING_FILEPATH',
                                                       "./template/tpl-located-service-offering.json")
TEMPLATE_COMPLIANCE_REFERENCE_MANAGER_FILEPATH = os.getenv('TEMPLATE_COMPLIANCE_REFERENCE_MANAGER_FILEPATH',
                                                   "./template/tpl-compliance-reference-manager.json")
TEMPLATE_COMPLIANCE_REFERENCE_FILEPATH = os.getenv('TEMPLATE_COMPLIANCE_REFERENCE_FILEPATH',
                                                   "./template/tpl-compliance-reference.json")
TEMPLATE_PROVIDER_FILEPATH = os.getenv('TEMPLATE_PROVIDER_FILEPATH',
                                                   "./template/tpl-provider.json")
TEMPLATE_COMLIANCE_CERT_SCHEME = os.getenv('TEMPLATE_COMLIANCE_CERT_SCHEME',
                                                   "./template/tpl-compliance-certification-scheme.json")


DIRECTORY_FOR_LOCATION = os.getenv('DIRECTORY_FOR_LOCATION', "/location/")
DIRECTORY_FOR_SERVICE_OFFERING = os.getenv('DIRECTORY_FOR_SERVICE_OFFERING', "/service-offering/")
DIRECTORY_FOR_LOCATED_SERVICE_OFFERING = os.getenv('DIRECTORY_FOR_LOCATED_SERVICE_OFFERING',
                                                           "/located-service-offering/")
DIRECTORY_FOR_COMPLIANCE_REFERENCE = os.getenv('DIRECTORY_FOR_COMPLIANCE_REFERENCE',
                                                       "/compliance-reference/")
DIRECTORY_FOR_COMPLIANCE_CERT_SCHEME = os.getenv('DIRECTORY_FOR_COMPLIANCE_CERT_SCHEME',
                                                       "/compliance-certification-scheme/")
DIRECTORY_FOR_COMPLIANCE_CERT_CRED = os.getenv('DIRECTORY_FOR_COMPLIANCE_CERT_CRED',
                                                       "/compliance-certification-credential/")
DIRECTORY_FOR_THIRD_PARTY_COMPLIANCE_CERT_CRED = os.getenv('DIRECTORY_FOR_THIRD_PARTY_COMPLIANCE_CERT_CRED',
                                                       "/third-party-compliance-certification-credential/")

ALLOWED_CRED_DIRECTORIES = {DIRECTORY_FOR_COMPLIANCE_CERT_CRED, DIRECTORY_FOR_THIRD_PARTY_COMPLIANCE_CERT_CRED}
ALLOWED_CRED_TYPES = {x.replace('/', '') for x in ALLOWED_CRED_DIRECTORIES}

PUBLIC_FOLDER_TO_EXPOSE = os.getenv('PUBLIC_FOLDER_TO_EXPOSE', "./public/")
TMP_FOLDER = os.getenv("TMP_FOLDER", "./tmp")

CATALOG_API_ENDPOINT = os.getenv('CATALOG_API_ENDPOINT')

TLS_CERT_PATH = os.environ.get('TLS_CERT_PATH')

PARTICIPANT_HASH = hashlib.sha256(bytes(PARTICIPANT_NAME,'utf-8')).hexdigest()

OUT_FOLDER = os.getenv('OUT_FOLDER', './public')
PARTICIPANT_FOLDER = os.path.join(OUT_FOLDER, 'participant')

PARENT_DOMAIN = os.getenv('PARENT_DOMAIN', 'provider.gaia-x.community')
SERVER_NAME = PARTICIPANT_NAME + '.' + PARENT_DOMAIN

DID_ROOT = "did:web:%s.%s"%(PARTICIPANT_NAME, PARENT_DOMAIN)
DID_URL = f"{DID_ROOT}:participant"



VC_ISSUER_URL = os.getenv('VC_ISSUER_URL', 'http://vc-issuer')

logging.getLogger().setLevel(logging.DEBUG)