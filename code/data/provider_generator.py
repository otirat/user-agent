import logging
from catalog_from_csv import utils

compliance_assessment_body = {
    'lne': {
        'designation': 'LNE',
        'registrationNumber': '31332024400012',
        'legalAddress': {
            'country-name': 'FR',
            'street-address': '1 rue Gaston Boissier',
            'locality': 'Paris',
            'postal-code': '75015'
        },
        'leiCode': 'FR92313320244',
        'termsAndConditions': 'https://www.lne.fr/sites/default/files/bloc-telecharger/regles-usages-marques-certification-LNE.pdf'
    },
    'sap': {
        'designation': 'SAP',
        'registrationNumber': 'RA000296',
        'legalAddress': {
            'country-name': 'DE',
            'street-address': 'Dietmar-Hopp-Allee 16',
            'locality': 'Walldorf',
            'postal-code': '69190'
        },
        'leiCode': '529900D6BF99LW9R2E68',
        'termsAndConditions': 'https://www.sap.com/france/about/trust-center/agreements/on-premise/general-terms-and-conditions.html'
    },
    'kpmg': {
        'designation': 'KPMG',
        'registrationNumber': 'RA000432',
        'legalAddress': {
            'country-name': 'LU',
            'street-address': '39, avenue John F. Kennedy',
            'locality': 'Luxembourg',
            'postal-code': '1855'
        },
        'leiCode': '549300YLGBTC4FWV0388',
        'termsAndConditions': 'https://assets.kpmg/content/dam/kpmg/no/pdf/2021/02/Standard-terms-and-conditions-2021.pdf'
    },
}

compliance_reference_manager_content = {
    'abc-federation': {
        'designation': 'abc-federation',
        'registrationNumber': '1234567890',
        'legalAddress': {
            'country-name': 'BE',
            'street-address': 'Avenue des Arts 6-9',
            'locality': 'Bruxelles',
            'postal-code': '1210'
        },
        'leiCode': 'abc-federation',
        'termsAndConditions': 'https://abc-federation.gaia-x.community/terms'
    },
}

provider_content = {
    'aruba': {
        'designation': 'ARUBA S.P.A.',
        'registrationNumber': 'ITRI.04552920482',
        'legalAddress': {
            'country-name': 'IT-BG',
            'street-address': 'via San Clemente, 53',
            'locality': 'Ponte San Pietro',
            'postal-code': '24036'
        },
        'leiCode': '81560069E326FF3BE121',
        'termsAndConditions': 'https://hosting.aruba.it/en/terms-conditions.aspx'
    },
    'aws': {
        'designation': 'AWS',
        'registrationNumber': 'RA000602',
        'legalAddress': {
            'country-name': 'LUX',
            'street-address': '22 Rue Edward Steichen',
            'locality': 'Luxembourg',
            'postal-code': '2540'
        },
        'leiCode': '549300VRR0S0ANY77J68',
        'termsAndConditions': 'https://aws.amazon.com/fr/terms/?nc1=f_pr'
    },
    'coretech': {
        'designation': 'CORETECH',
        'registrationNumber': '39078',
        'legalAddress': {
            'country-name': 'IT',
            'street-address': 'Viale Ortles 13',
            'locality': 'Milano',
            'postal-code': '20139'
        },
        'leiCode': '23456UY87654',
        'termsAndConditions': 'https://www.coretech.it/fr/service/chi_siamo/condizioni_generali.php'
    },
    'gigas': {
        'designation': 'GIGAS',
        'registrationNumber': 'CDP17D020006ESGIGA',
        'legalAddress': {
            'country-name': 'ES',
            'street-address': 'Avenida de Fuencarral 44',
            'locality': 'Alcobendas',
            'postal-code': '28108'
        },
        'leiCode': 'CDP17D020006ESGIGA',
        'termsAndConditions': 'https://gigas.com/en/legal-condiciones-uso.html'
    },
    'ikoula': {
        'designation': 'IKOULA',
        'registrationNumber': 'B 417 680 618',
        'legalAddress': {
            'country-name': 'FR',
            'street-address': "175-177 rue d'Aguesseau",
            'locality': 'Boulogne Billancourt',
            'postal-code': '92100'
        },
        'leiCode': 'IKOULA',
        'termsAndConditions': 'https://www.ikoula.com/fr/conditions-utilisations'
    },
    'irideos': {
        'designation': 'IRIDEOS',
        'registrationNumber': 'RA000407',
        'legalAddress': {
            'country-name': 'IT',
            'street-address': "VIALE BODIO LUIGI, 33/39",
            'locality': 'MILANO',
            'postal-code': '20158'
        },
        'leiCode': '81560021169E898FFC87',
        'termsAndConditions': 'https://irideos.it/note-legali/'
    },
    'jotelulu': {
        'designation': 'JOTELULU',
        'registrationNumber': 'B65814709',
        'legalAddress': {
            'country-name': 'ES',
            'street-address': "plaza Pablo Ruiz Picasso, n.º 1",
            'locality': 'Madrid',
            'postal-code': '28020'
        },
        'leiCode': 'JOTELULU, S.L.',
        'termsAndConditions': 'https://jotelulu.com/aviso-legal/'
    },
    'leaseweb': {
        'designation': 'LEASEWEB',
        'registrationNumber': '456789OIUH',
        'legalAddress': {
            'country-name': 'NL',
            'street-address': "Hessenbergweg 95",
            'locality': 'Amsterdam',
            'postal-code': '1101'
        },
        'leiCode': 'leaseweb',
        'termsAndConditions': 'https://www.leaseweb.com/legal'
    },
    'netalia': {
        'designation': 'NETALIA',
        'registrationNumber': 'GE 452404',
        'legalAddress': {
            'country-name': 'IT',
            'street-address': "Piazza della Vittoria, 11A",
            'locality': 'Genova',
            'postal-code': '16121'
        },
        'leiCode': 'netalia',
        'termsAndConditions': 'https://www.netalia.it/legal'
    },
    'opiquad': {
        'designation': 'OPIQUAD',
        'registrationNumber': 'MI - 1946731',
        'legalAddress': {
            'country-name': 'IT',
            'street-address': "Via Bergamo, 60",
            'locality': 'Merate',
            'postal-code': '23807'
        },
        'leiCode': '8156000C6C0C6B541617',
        'termsAndConditions': 'https://www.opiquad.it/documenti/'
    },
    'ovhcloud': {
        'designation': 'OVHCLOUD',
        'registrationNumber': '424 761 419 00045',
        'legalAddress': {
            'country-name': 'FR',
            'street-address': '2 Rue Kellermann',
            'locality': 'Roubaix',
            'postal-code': '59100'
        },
        'leiCode': '969500Q2MA9VBQ8BG884',
        'termsAndConditions': 'https://www.ovhcloud.com/fr/terms-and-conditions/'
    },
    'montblanc-iot': {
        'designation': 'Mont Blanc IOT',
        'registrationNumber': '123456789',
        'legalAddress': {
            'country-name': 'FR',
            'street-address': "Place de l'aiguille du midi",
            'locality': 'Chamonix',
            'postal-code': '74400'
        },
        'leiCode': '',
        'termsAndConditions': 'https://www.example.com/montblanc-iot/terms-and-conditions/'
    },
    'dufourstorage': {
        'designation': 'Dufour Storage',
        'registrationNumber': '123456789',
        'legalAddress': {
            'country-name': 'IT',
            'street-address': 'Via Europa',
            'locality': 'Milano',
            'postal-code': '20121'
        },
        'leiCode': '',
        'termsAndConditions': 'https://www.example.com/dufourstorage/terms-and-conditions/'
    }
}


class ParticipantGenerator:

    def __init__(self, template_provider: str,
                 hash: str, participant_name: str,
                 base_did_url_provider: str,
                 did_web: str, output_folder: str):
        self.template_participant = utils.load_template(template_provider)
        self.hash = hash
        self.name = participant_name
        self.did_web = did_web
        self.base_did_url_provider = base_did_url_provider
        self.output_folder = output_folder

    @staticmethod
    def isProvider(name: str):
        return provider_content.get(name) != None

    @staticmethod
    def isComplianceReferenceManager(name: str):
        return compliance_reference_manager_content.get(name) != None

    @staticmethod
    def isComplianceAssessmentBody(name: str):
        return compliance_assessment_body.get(name) != None

    def forge_id(self):
        return self.base_did_url_provider + ':' + self.hash + "/data.json"

    def generate(self):
        id = self.forge_id()

        content = None
        if ParticipantGenerator.isProvider(self.name):
            json_type = "gax-participant:Provider"
            content = provider_content.get(self.name, None)

        if ParticipantGenerator.isComplianceReferenceManager(self.name):
            json_type = "gax-participant:ComplianceReferenceManager"
            content = compliance_reference_manager_content.get(self.name, None)

        if ParticipantGenerator.isComplianceAssessmentBody(self.name):
            json_type = "gax-participant:ComplianceAssessmentBody"
            content = compliance_assessment_body.get(self.name, None)

        if not content: raise AttributeError(f"No provider content for {self.name}")

        didWeb = self.did_web

        designation = content['designation']
        registrationNumber = content['registrationNumber']
        legalAddress = content['legalAddress']
        leiCode = content['leiCode']
        termsAndConditions = content['termsAndConditions']
        self.data = self.to_json_ld(id, didWeb, designation, json_type, registrationNumber, legalAddress, leiCode,
                                    termsAndConditions)

        utils.save_on_disk(self.output_folder, "data.json", self.data)

    def get_id(self):
        return self.data["@id"]

    def to_json_ld(self, id, didWeb, designation, json_type, registrationNumber, legalAddress, leiCode,
                   termsAndConditions):
        data = self.template_participant.copy()

        data["@id"] = id
        data["gax-participant:hasDidWeb"]["@value"] = didWeb
        data["@type"] = json_type
        data["gax-participant:registrationNumber"]["@value"] = registrationNumber

        data["gax-participant:legalAddress"]["vcard:country-name"]["@value"] = legalAddress['country-name']
        data["gax-participant:legalAddress"]["vcard:street-address"]["@value"] = legalAddress['street-address']
        data["gax-participant:legalAddress"]["vcard:postal-code"]["@value"] = legalAddress['postal-code']
        data["gax-participant:legalAddress"]["vcard:locality"]["@value"] = legalAddress['locality']

        # For the demo, Head quarter address is also the Legal Address
        data["gax-participant:headquarterAddress"]["vcard:country-name"]["@value"] = legalAddress['country-name']
        data["gax-participant:headquarterAddress"]["vcard:street-address"]["@value"] = legalAddress['street-address']
        data["gax-participant:headquarterAddress"]["vcard:postal-code"]["@value"] = legalAddress['postal-code']
        data["gax-participant:headquarterAddress"]["vcard:locality"]["@value"] = legalAddress['locality']

        data["gax-participant:designation"]["@value"] = designation
        data["gax-participant:leiCode"]["@value"] = leiCode

        data["gax-participant:termsAndConditions"]["gax-core:Value"]["@value"] = termsAndConditions
        data["gax-participant:termsAndConditions"]["gax-core:hash"]["@value"] = utils.computesha256(termsAndConditions)

        return data
