import os
from catalog_from_csv import utils

compliance_reference_manager_content = {
  'abc-federation': {
    'designation': 'abc-federation',
    'registrationNumber': '1234567890',
    'legalAddress': {
      'country-name': 'BE',
      'street-address': 'Avenue des Arts 6-9',
      'locality': 'Bruxelles',
      'postal-code': '1210'
    },
    'leiCode': 'abc-federation',
    'termsAndConditions': 'https://abc-federation.gaia-x.community/terms'
  },
}


class ComplianceReferenceManagerGenerator:
  
  filename = 'data.json'
  
  def __init__(self, template: str,
                participant_hash: str,
                participant_name: str,
                base_did_url_provider: str, 
                output_folder: str):
    self.template_participant = utils.load_template(template)
    self.participant_hash = participant_hash
    self.name = participant_name
    self.base_did_url_provider = base_did_url_provider
    self.output_folder = output_folder
    self.map_compliance_reference_id = []
   
  def forge_id(self):
    return self.base_did_url_provider+':'+self.participant_hash+"/data.json"
  
  def get_filepath(self):
    return os.path.join(self.output_folder, self.filename)
  
  def generate(self):
    output_location_directory = self.output_folder+'/'+self.participant_hash
    
    id = self.forge_id()
  
    content = compliance_reference_manager_content[self.name]
      
    did_web = self.base_did_url_provider+'/did.json'
     
    registrationNumber = content['registrationNumber']
    legalAddress = content['legalAddress']
    leiCode  = content['leiCode']
    termsAndConditions  = content['termsAndConditions']
    self.data = self.to_json_ld(id, did_web, registrationNumber, legalAddress, leiCode, termsAndConditions)
    
    utils.save_on_disk_by_filepath(self.get_filepath(), self.data)

  def get_id(self):
    return self.data["@id"]
  
  def associate_compliance_reference(self, compliance_reference_did):
    self.map_compliance_reference_id.append(compliance_reference_did)
    
    compliance_references = self.data.get("gax-compliance:hasComplianceReferences", [])
    compliance_references.append({"@id": compliance_reference_did, "@type":"gax-compliance:ComplianceReference"})
    
    self.data["gax-compliance:hasComplianceReferences"] =  compliance_references
    utils.save_on_disk_by_filepath(self.get_filepath(), self.data)
    
  def to_json_ld(self, id, did_web, registrationNumber, legalAddress, leiCode, termsAndConditions):
    data = self.template_participant.copy()
    
    data["@id"] = id
    data["gax-participant:hasDidWeb"]["@value"] = did_web
    data["gax-participant:registrationNumber"]["@value"] = registrationNumber
    
    data["gax-participant:legalAddress"]["vcard:country-name"]["@value"] = legalAddress['country-name']
    data["gax-participant:legalAddress"]["vcard:street-address"]["@value"] = legalAddress['street-address']
    data["gax-participant:legalAddress"]["vcard:postal-code"]["@value"] = legalAddress['postal-code']
    data["gax-participant:legalAddress"]["vcard:locality"]["@value"] = legalAddress['locality']
    
    # For the demo, Head quarter address is also the Legal Address            
    data["gax-participant:headquarterAddress"]["vcard:country-name"]["@value"] = legalAddress['country-name']
    data["gax-participant:headquarterAddress"]["vcard:street-address"]["@value"] = legalAddress['street-address']
    data["gax-participant:headquarterAddress"]["vcard:postal-code"]["@value"] = legalAddress['postal-code']
    data["gax-participant:headquarterAddress"]["vcard:locality"]["@value"] = legalAddress['locality']
    
    data["gax-participant:leiCode"]["@value"] = leiCode
    
    data["gax-participant:termsAndConditions"]["gax-core:Value"]["@value"] = termsAndConditions
    data["gax-participant:termsAndConditions"]["gax-core:hash"]["@value"] = utils.computesha256(termsAndConditions)      
    
    return data