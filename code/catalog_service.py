import json
import Config
import requests
import logging
import traceback


def bootstrap_catalog(catalog_file):
    try:
        f = open(catalog_file, "r")
        catalog_data = json.load(f)
        f.close()

        if not Config.CATALOG_API_ENDPOINT: raise AttributeError("CATALOG_API_ENDPOINT is not defined")
        url = Config.CATALOG_API_ENDPOINT + "/json-ld"
        payload = json.dumps(catalog_data).encode('utf-8')
        headers = {
            'Content-Type': 'application/json; charset=utf-8'
        }

        response = requests.request("POST", url, headers=headers, data=payload)

        logging.info(f"Data posted for catalog and response is {response.text}")

        return response.status_code
    except Exception as e:
        logging.error(f"Unable to index catalog for bootstrap because {traceback.format_exc()}")
