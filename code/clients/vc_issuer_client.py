import hashlib
import requests
import json
import Config
import logging

VC_ISSUER_URL = Config.VC_ISSUER_URL
ISSUER = Config.DID_ROOT


def to_vc(json_ld: dict, type: str):
    sha256_hash = hashlib.sha256(bytes(json_ld.__str__(), 'utf-8'))
    hash = sha256_hash.hexdigest()
    id = f"https://{Config.SERVER_NAME}/participant/{Config.PARTICIPANT_HASH}/vc/{hash}/data.json"
    vc =  {
            "@context": ["https://www.w3.org/2018/credentials/v1"],
            "@type": ["VerifiableCredential", f"{type}"],
            "@id": id,
            "issuer": f"{ISSUER}",
            "issuanceDate": "2022-09-23T23:23:23.235Z",
        }
    vc['credentialSubject'] = json_ld
    return vc

def put_json_ld_content(json_ld):
    headers = {"Content-Type": "application/json"}

    type = json_ld["@type"].split(':').pop()
    content = to_vc(json_ld, type)
    
    logging.info("CONTENT\n\n\n")
    logging.info(content)
    
    r = requests.put(VC_ISSUER_URL + "/api/v0.9/sign",
                      headers=headers,
                      json=content)
    logging.info(r.text)

    return r
