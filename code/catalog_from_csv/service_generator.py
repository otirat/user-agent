import Config
from catalog_from_csv import utils
from catalog_from_csv.location_generator import LocationGenerator
import csv
import hashlib
import os
import logging
import copy


class ServiceGenerator:
    filename = 'data.json'

    service_offering_map = {}
    
    def __init__(self, template_service_offering, template_located_service_offering, provider_base_did,
                 directory_for_service_offering, directory_for_located_service_offering, participant_hash, out_folder):
        self.template_service_offering = utils.load_template(template_service_offering)
        self.template_located_service_offering = utils.load_template(template_located_service_offering)

        self.provider_base_did = provider_base_did

        self.directory_for_service_offering = directory_for_service_offering
        self.directory_for_located_service_offering = directory_for_located_service_offering

        self.participant_hash = participant_hash
        self.out_folder = out_folder
        self.map_location_id_service_offering = {}
        self.map_location_id_located_service_offering = {}

    def build_service_offering_did_web(self, designation):
        hash = hashlib.sha256(bytes(designation, 'utf-8')).hexdigest()
        return self.provider_base_did + self.directory_for_service_offering + hash + '/data.json'

    def build_located_service_offering_did_web(self, designation):
        hash = hashlib.sha256(bytes(designation, 'utf-8')).hexdigest()
        return self.provider_base_did + self.directory_for_located_service_offering + hash + '/data.json'

    def generate_service(self, provider_did_url, csv_file_path, location_generator):
        output_service_offering_directory = self.out_folder + '/' + self.participant_hash + self.directory_for_service_offering
        output_located_service_offering_directory = self.out_folder + '/' + self.participant_hash + self.directory_for_located_service_offering
        utils.clean_output_directories([output_service_offering_directory, output_located_service_offering_directory])

        with open(csv_file_path, newline='') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=";")

            for row in reader:
                service_id = self.get_value_of(row, 'Service Id')
                designation = self.get_value_of(row, 'Designation')
                service_version = self.get_value_of(row, 'Service Version')
                complies = self.get_value_of(row, 'Comply With')
                availables = self.get_value_of(row, 'Available In')
                description = self.get_value_of(row, 'Description')
                service_type = self.get_value_of(row, 'Service Type')
                layer = self.get_value_of(row, 'Layer')
                keyword = self.get_value_of(row, 'Keyword')

                # Service Type;Layer
                # "Compute;Container;Data Analytics;Data Base;Networking;Storage;Virtual Machine;VPN;Webhosting";"IAAS;SAAS"

                service_offering = self.build_service_offering(provider_did_url, service_id, designation,
                                                               service_version, complies, availables, service_type,
                                                               description, layer, keyword,
                                                               location_generator)
                
                location_generator.associate_service_offering(self.map_location_id_service_offering)

                located_service_offerings = self.build_located_service_offerings(service_id, availables,
                                                                                 location_generator)
                location_generator.associate_located_service_offering(self.map_location_id_located_service_offering)

                # ID is based on hash of 'Service Id' for service offering and located service offering
                hash = hashlib.sha256(bytes(service_id, 'utf-8')).hexdigest()
                output_filepath = os.path.join(output_service_offering_directory, hash, self.filename)
                utils.save_on_disk_by_filepath(output_filepath, service_offering)
                self.service_offering_map[row['Service Id']] = {'data': copy.deepcopy(service_offering), 'filepath': output_filepath}

                for located_service_offering in located_service_offerings:
                    logging.info("Located SVC id %s", located_service_offering["@id"])
                    items = located_service_offering["@id"].split('/')
                    hash = items[len(items) - 2]

                    output_filepath = os.path.join(output_located_service_offering_directory, hash, self.filename)
                    utils.save_on_disk_by_filepath(output_filepath, located_service_offering)

    def set_layer(self, csv_file_path):
        with open(csv_file_path, newline='') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=";")

            for row in reader:
                layer = row['Layer']
                ids = row['Service ID']
                service_ids = [i.strip() for i in ids.split(';')] if ids else []
                for id in service_ids:
                    if len(id) != 0:
                        self.service_offering_map[id]['data']['gax-service:layer'] = self.build_values_from_string(layer)
                        utils.save_on_disk_by_filepath(self.service_offering_map[id]['filepath'], self.service_offering_map[id]['data'])
        
    def set_keywords(self, csv_file_path):
        with open(csv_file_path, newline='') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=";")

            svc_ids_to_keywords = {}
            for row in reader:
                keyword = row['Keyword'].strip()
                ids = row['Service IDs']
                service_ids = [i.strip() for i in ids.split(';')] if ids else []
                for id in service_ids:
                    if len(id) != 0:
                        keywords = svc_ids_to_keywords.get(id, [])
                        keywords.append(keyword)
                        svc_ids_to_keywords[id] = keywords
                        
            for id, keywords in svc_ids_to_keywords.items():
                self.service_offering_map[id]['data']['gax-service:serviceType'] = self.build_values_from_string(";".join(keywords))
                self.service_offering_map[id]['data']['dcat:keyword'] = self.build_values_from_string(";".join(keywords))
                utils.save_on_disk_by_filepath(self.service_offering_map[id]['filepath'], self.service_offering_map[id]['data'])
    
    def get_value_of(self, row, field):
        return row[field] if field in row else ""

    def build_service_offering(self, provider_did, provider_service_id, designation, service_version, complies,
                               availables, service_type, description, layer, keyword,
                               location_generator: LocationGenerator):
        data = copy.deepcopy(self.template_service_offering)

        data['@id'] = self.build_service_offering_did_web(provider_service_id)
        data['gax-service:providedBy'] = {
            "@id": provider_did
        }
        data['gax-service:termsAndConditions'] = {
            "@type": "gax-core:TermsAndConditions",
            "gax-core:Value": {
                "@value": "https://www.example.com/terms-and-conditions/",
                "@type": "xsd:anyURI"
            },
            "gax-core:hash": {
                "@value": "31296ef8727fc63e89213f3e5ab928bae7699b6dade0af5e443b8ad2623639ee8e6176696a1fa125ccfa7fa55465463b6424d3f54435d6a2beafd79a91425b0a",
                "@type": "xsd:string"
            }
        }
        data['gax-service:serviceTitle'] = {
            "@value": designation,
            "@type": "xsd:string"
        }
        data['gax-service:serviceType'] = self.build_values_from_string(service_type)
        data['gax-service:layer'] = self.build_values_from_string(layer)
        data['gax-service:providerServiceId'] = {
            "@value": provider_service_id,
            "@type": "xsd:string"
        }
        data['dct:description'] = self.build_description(description, designation)
        data['dcat:keyword'] = self.build_values_from_string(keyword)

        available_locations = []
        for available in availables.split(';'):
            if available.strip() != "":
                available_locations.append({
                    "@id": location_generator.get_did_web_associated_to_location_id(available.strip())
                })
                self.remember_association_location_id_and_service_offering(available.strip(), data['@id'])
        data['gax-service:isAvailableOn'] = available_locations

        return data

    def build_located_service_offerings(self, service_id, availables, location_generator: LocationGenerator):
        datas = []

        for available in availables.split(';'):
            if available.strip() != "":
                data = copy.deepcopy(self.template_located_service_offering)

                data['gax-service:isImplementationOf'] = self.build_service_offering_did_web(service_id)
                data['@id'] = self.build_located_service_offering_did_web(service_id + "_" + available)
                data['gax-service:isImplementationOf'] = self.build_service_offering_did_web(service_id)
                data['gax-service:isHostedOn'] = {
                    "@id": location_generator.get_did_web_associated_to_location_id(available.strip())
                }

                self.remember_association_location_id_and_located_service_offering(available.strip(), data['@id'])

                datas.append(data)

        return datas

    def remember_association_location_id_and_service_offering(self, location_id, service_offering_did):
        if location_id in self.map_location_id_service_offering:
            self.map_location_id_service_offering[location_id].append(service_offering_did)
        else:
            self.map_location_id_service_offering[location_id] = [service_offering_did]

    def remember_association_location_id_and_located_service_offering(self, location_id, located_service_offering_did):
        if location_id in self.map_location_id_located_service_offering:
            self.map_location_id_located_service_offering[location_id].append(located_service_offering_did)
        else:
            self.map_location_id_located_service_offering[location_id] = [located_service_offering_did]

    def build_description(self, description, designation):
        if description == "":
            return f"{designation} is a service offered by {Config.PARTICIPANT_NAME.upper()}"
        else:
            return description

    def build_values_from_string(self, string):
        values = []

        if string == "":
            return {
                "@value": "",
                "@type": "xsd:string"
            }

        for value in string.split(';'):
            if value.strip() != "":
                values.append({
                    "@value": value.strip(),
                    "@type": "xsd:string"
                })

        return values
