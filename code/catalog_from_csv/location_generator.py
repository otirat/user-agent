import logging
from catalog_from_csv import administrative_location_utils
from catalog_from_csv import utils
import csv
import json
import hashlib
import os
import copy

class LocationGenerator:

    filename = 'data.json'
    
    def __init__(self, template_location, provider_base_did, directory_for_location, participant_hash, out_folder):
        self.template = utils.load_template(template_location)
        self.provider_base_did = provider_base_did
        self.directory_for_location = directory_for_location
        self.participant_hash = participant_hash
        self.out_folder = out_folder
        self.map_location_id_did_web = {}

    def get_did_web_associated_to_location_id(self, location_id):
        logging.info("get_did_web_associated_to_location_id location_id %s", location_id)
        logging.info("self.map_location_id_did_web %s", self.map_location_id_did_web)
        if location_id in self.map_location_id_did_web:
            return self.map_location_id_did_web[location_id]['did_web']
        else:
            return None

    def associate_service_offering(self, map_service_offering_location_id):
        for location_id in map_service_offering_location_id.keys():
            if location_id in self.map_location_id_did_web:
                location_path = self.map_location_id_did_web[location_id]['location_path']
                service_offering_dids = map_service_offering_location_id[location_id]

                with open(location_path) as location_file:
                    location = json.load(location_file)
                    location_file.close()
                    service_offerings = []

                    for service_offering_did in service_offering_dids:
                        service_offerings.append({
                            "@id": service_offering_did
                        })
                    location['gax-service:canHostServiceOffering'] = service_offerings
                    utils.save_on_disk_by_filepath(location_path, location)
            else:
                print(f"Unknown location with location id : {location_id}")

    def associate_located_service_offering(self, map_located_service_offering_location_id):
        for location_id in map_located_service_offering_location_id.keys():
            if location_id in self.map_location_id_did_web:
                location_path = self.map_location_id_did_web[location_id]['location_path']
                located_service_offering_dids = map_located_service_offering_location_id[location_id]

                with open(location_path) as location_file:
                    location = json.load(location_file)
                    location_file.close()
                    located_service_offerings = []

                    for located_service_offering_did in located_service_offering_dids:
                        located_service_offerings.append({
                            "@id": located_service_offering_did
                        })
                    location['gax-participant:hasLocatedServiceOffering'] = located_service_offerings
                    utils.save_on_disk_by_filepath(location_path, location)
            else:
                print(f"Unknown location with location id : {location_id}")
  
    def generate_location(self, provider_did, csv_file_path):
        output_location_directory = self.out_folder+'/'+self.participant_hash+self.directory_for_location
        utils.clean_output_directory(output_location_directory)

        logging.debug("Generate Location to %s",output_location_directory)
        
        with open(csv_file_path, newline='') as csvfile:
            data = copy.deepcopy(self.template)

            reader = csv.DictReader(csvfile, delimiter=";")
            for row in reader:
                designation = row['Location Id']
                hash = hashlib.sha256(bytes(designation,'utf-8')).hexdigest()
                did_web = self.provider_base_did + self.directory_for_location + hash + '/'+self.filename
                output_filepath = os.path.join(output_location_directory,hash, self.filename)
                
                department = row['State/Dpt']
                country = row['Country']

                data['@id'] = did_web
                data['gax-participant:hasProvider'] = {
                    "@id": provider_did
                }
                data['gax-participant:hasAdministrativeLocation'] = {
                    "@value": administrative_location_utils.find_administrative_location(country, department),
                    "@type": "xsd:string"
                }
                data['gax-participant:providerDesignation'] = {
                    "@value": row['Provider Designation'],
                    "@type": "xsd:string"
                }
                data['gax-participant:country'] = {
                    "@value": row['Country'],
                    "@type": "xsd:string"
                }

                data['gax-participant:state'] = {
                    "@value": row['State/Dpt'],
                    "@type": "xsd:string"
                }

                data['gax-participant:urbanArea'] = {
                    "@value": row['City'],
                    "@type": "xsd:string"
                }

                self.map_location_id_did_web[row['Location Id']] = {
                    "did_web": did_web,
                    "location_path": output_filepath
                }
                utils.save_on_disk_by_filepath(output_filepath, data)
