import json
import os
import shutil
import logging
import traceback


def save_on_disk(directory, filename, data):
    save_on_disk_by_filepath(os.path.join(directory, filename), data)

def save_on_disk_by_filepath(filepath, data):
    dirname = os.path.dirname(filepath)
    os.path.isdir(dirname) or os.makedirs(dirname)
    result_file = open(filepath, "w", encoding='utf8')
    result_file.write(json.dumps(data, indent=4))
    result_file.close()


def clean_output_directory(directory):
    try:
        shutil.rmtree(directory)
    except FileNotFoundError:
        pass

    os.makedirs(directory)


def clean_output_directories(directories):
    try:
        for d in directories:
            shutil.rmtree(d)
    except FileNotFoundError:
        pass

    for d in directories:
        os.mkdir(d)


def load_template(template_path):
    # load template
    tpl = open(template_path)
    template = json.load(tpl)
    tpl.close()

    return template


def extract_filename_of_did(did):
    """
    :param did:
    Example : did:web:abc-federation.gaia-x.community/public/json-ld/participant.json
    :return: participant.json
    """
    split = did.split("/")

    return split[len(split) - 1]

def computesha256(url: str):
    import hashlib
    import requests
    
    hash = ""
    try:
        r = requests.get(url, stream=True, allow_redirects=True)
        sha256_hash = hashlib.sha256(r.content)
        hash = sha256_hash.hexdigest()
    except:
        logging.error(traceback.format_exc)
    return hash
    
def date_to_xds(date: str):
    from datetime import datetime
    return datetime.strptime(date, '%m/%d/%y').isoformat()
