import logging
import os

import Config
import build_catalog
import catalog_service
from catalog_from_csv.compliance_reference_generator import ComplianceReferenceGenerator, CertificationSchemeType
from catalog_from_csv.location_generator import LocationGenerator
from catalog_from_csv.service_generator import ServiceGenerator
from catalog_from_csv.compliance_certification_scheme_generator import ComplianceCertificationSchemeGenerator
from data.compliance_reference_manager_generator import ComplianceReferenceManagerGenerator
from data.provider_generator import ParticipantGenerator
from did_document.did_document import DIDDocumentGenerator


class CatalogGenerator:

    def __init__(self):
        pass

    def generate_catalog(self, location_filename, service_filename, compliance_ref_filename, layers_filename, keywords_filename):
        location_generator = None

        self.create_participant_folder()
        provider_generator = self.generate_provider()

        provider_did = provider_generator.get_id()
        provider_base_did = Config.DID_URL + ":" + Config.PARTICIPANT_HASH

        location_generator = self.generate_location(location_filename, location_generator, provider_base_did,
                                                    provider_did)
        self.generate_service(location_generator, provider_base_did, provider_did, service_filename, layers_filename, keywords_filename)


        self.generate_compliance_ref(compliance_ref_filename, provider_base_did, provider_did)

        self.index_into_federated_catalog()

    def index_into_federated_catalog(self):
        build_catalog.launch_generation_from_directory(os.path.join(Config.PARTICIPANT_FOLDER, Config.PARTICIPANT_HASH),
                                                       os.path.join(Config.TMP_FOLDER, "generated_catalog.json"))

        if catalog_service.bootstrap_catalog(os.path.join(Config.TMP_FOLDER, "generated_catalog.json")) != 200:
            raise Exception("Unable to index catalog")

    def generate_compliance_ref(self, compliance_ref_filename, provider_base_did, provider_did):
        if compliance_ref_filename:
            logging.info("Start generation of Compliance Reference")

            compliance_ref_manager = ComplianceReferenceManagerGenerator(
                template=Config.TEMPLATE_COMPLIANCE_REFERENCE_MANAGER_FILEPATH,
                participant_hash=Config.PARTICIPANT_HASH,
                participant_name=Config.PARTICIPANT_NAME,
                base_did_url_provider=Config.DID_URL,
                output_folder=Config.PARTICIPANT_FOLDER + '/' + Config.PARTICIPANT_HASH)

            compliance_ref_manager.generate()

            compliance_ref_generator = ComplianceReferenceGenerator(
                template_compliance_reference=Config.TEMPLATE_COMPLIANCE_REFERENCE_FILEPATH,
                provider_base_did=provider_base_did,
                directory_for_compliance_reference=Config.DIRECTORY_FOR_COMPLIANCE_REFERENCE,
                participant_hash=Config.PARTICIPANT_HASH,
                out_folder=Config.PARTICIPANT_FOLDER)

            compliance_ref_generator.generate_reference(provider_did,
                                                        compliance_ref_filename,
                                                        compliance_reference_manager=compliance_ref_manager)

            for key, value in compliance_ref_generator.compliance_reference_map.items():
                logging.info(f"Process CR {key} - {value}")

                if value['type'] == CertificationSchemeType.SELF_DECLARED:
                    # Create Certification Scheme
                   compliance_cert_scheme_generator = ComplianceCertificationSchemeGenerator(template_cert_scheme=Config.TEMPLATE_COMLIANCE_CERT_SCHEME,
                        provider_base_did=provider_base_did,
                        directory_for_cert_scheme=Config.DIRECTORY_FOR_COMPLIANCE_CERT_SCHEME,
                        participant_hash=Config.PARTICIPANT_HASH,
                        out_folder=Config.PARTICIPANT_FOLDER)
                   
                   compliance_cert_scheme_generator.generate(cr_id=key , compliance_reference=value['compliance_reference'], compliance_ref_generator=compliance_ref_generator)
                    # Update CR with Scheme
                    # Update Manager
         
            logging.info("Generation of Compliance reference is finished")

    def generate_service(self, location_generator, provider_base_did, provider_did, service_filename, layers_filename, keywords_filename):
        # Service generation
        if service_filename:
            logging.info("Start generation of Service")

            service_generator = ServiceGenerator(template_service_offering=Config.TEMPLATE_SERVICE_OFFERING_FILEPATH,
                                                 template_located_service_offering=Config.TEMPLATE_LOCATED_SERVICE_OFFERING_FILEPATH,
                                                 provider_base_did=provider_base_did,
                                                 directory_for_service_offering=Config.DIRECTORY_FOR_SERVICE_OFFERING,
                                                 directory_for_located_service_offering=Config.DIRECTORY_FOR_LOCATED_SERVICE_OFFERING,
                                                 participant_hash=Config.PARTICIPANT_HASH,
                                                 out_folder=Config.PARTICIPANT_FOLDER)

            service_generator.generate_service(provider_did, service_filename, location_generator)
            if layers_filename:
                service_generator.set_layer(layers_filename)
            if keywords_filename:
                service_generator.set_keywords(keywords_filename)
            logging.info("Generation of Service is finished")

    def generate_location(self, location_filename, location_generator, provider_base_did, provider_did):
        # Location generation
        if location_filename:
            logging.info("Start generation of Location")
            location_generator = LocationGenerator(template_location=Config.TEMPLATE_LOCATION_FILEPATH,
                                                   provider_base_did=provider_base_did,
                                                   directory_for_location=Config.DIRECTORY_FOR_LOCATION,
                                                   participant_hash=Config.PARTICIPANT_HASH,
                                                   out_folder=Config.PARTICIPANT_FOLDER)

            location_generator.generate_location(provider_did, location_filename)
            logging.info("Generation of Location is finished")
        return location_generator

    def generate_did(self):
        did_document = DIDDocumentGenerator(tls_cert_path=Config.TLS_CERT_PATH, server_name=Config.SERVER_NAME,participant_hash=Config.PARTICIPANT_HASH,
                                            out_folder=Config.OUT_FOLDER)
        if Config.TLS_CERT_PATH:
            did_document.create_files()
        return did_document.get_did()

    def generate_provider(self):

        did_web = self.generate_did()

        provider_generator = ParticipantGenerator(template_provider=Config.TEMPLATE_PROVIDER_FILEPATH,
                                                  hash=Config.PARTICIPANT_HASH,
                                                  participant_name=Config.PARTICIPANT_NAME,
                                                  base_did_url_provider=Config.DID_URL,
                                                  did_web=did_web,
                                                  output_folder=os.path.join(Config.PARTICIPANT_FOLDER,
                                                                             Config.PARTICIPANT_HASH))
        provider_generator.generate()

        return provider_generator

    def create_participant_folder(self):
        folder = os.path.join(Config.PARTICIPANT_FOLDER, Config.PARTICIPANT_HASH)
        os.path.isdir(folder) or os.makedirs(folder)
