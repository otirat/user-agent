import pycountry

ADMINISTRATIVE_LOCATIONS = {
    "France-Seine": "FR-75C",
    "France-Alsace": "FR-6AE",
    "United Kingdom-London": "GB-LND",
}


def find_administrative_location(country_name, department):
    """
    :param country_name:
    :param department:
    :return: ISO code of the administrative location if found or country alpha_2 code otherwise
    """

    if ADMINISTRATIVE_LOCATIONS.get(country_name + "-" + department) is not None:
        return ADMINISTRATIVE_LOCATIONS.get(country_name + "-" + department)

    admin_country = pycountry.countries.get(name=country_name)

    if admin_country is None:
        print(f"[WARNING] Unable to find administrative country with country-name {country_name}")
    else:
        subdivisions = list(pycountry.subdivisions.get(country_code=admin_country.alpha_2))

        admin_subdivision = None

        for subdivision in subdivisions:
            if subdivision.name == department:
                admin_subdivision = subdivision
                break
        if admin_subdivision is None:
            print(
                f"[WARNING] Unable to find administrative subdivision with country-name {country_name} and department {department}")
            return admin_country.alpha_2
        else:
            return admin_subdivision.code
