import enum
import logging
import os
from catalog_from_csv import utils

import csv
import hashlib
from catalog_from_csv.compliance_reference_generator import ComplianceReferenceGenerator


# Static -> 

class ComplianceCertificationSchemeGenerator:
                                                                
    def __init__(self, template_cert_scheme, provider_base_did,
                 directory_for_cert_scheme, participant_hash,out_folder):
        self.template = utils.load_template(template_cert_scheme)
        self.provider_base_did = provider_base_did
        self.directory_for_cert_scheme = directory_for_cert_scheme
        self.participant_hash = participant_hash
        self.out_folder = out_folder
        self.filename = 'data.json'

    def generate(self, cr_id, compliance_reference, compliance_ref_generator: ComplianceReferenceGenerator):
        output_directory = self.out_folder+'/'+self.participant_hash+self.directory_for_cert_scheme
        utils.clean_output_directory(output_directory)

        logging.info("Generate Compliance Certification scheme to %s",output_directory)
        
        logging.info(compliance_reference)
        
        hash = hashlib.sha256(bytes(cr_id,'utf-8')).hexdigest()
        did_web = self.provider_base_did + self.directory_for_cert_scheme + hash + '/'+self.filename
        
        scheme = self.to_json_ld(did_web, compliance_reference['@id'])
        
        output_filepath = os.path.join(output_directory, hash, self.filename)
                
        compliance_ref_generator.associate_certification_scheme(cr_id, scheme['@id'])        
        utils.save_on_disk_by_filepath(output_filepath, scheme)

    def to_json_ld(self, id, compliance_reference_web_id: str):
        data = self.template.copy()

        data['gax-compliance:hasComplianceReference']["@value"] = compliance_reference_web_id
        # Should create 
        data['gax-compliance:grantsComplianceCriteriaCombination']['@value'] = ""
        data['@id'] = id

        return data
