import enum
import logging
import os
from catalog_from_csv import utils
from data.compliance_reference_manager_generator import ComplianceReferenceManagerGenerator
import csv
import hashlib
import traceback
from enum import Enum
import copy

class CertificationSchemeType(Enum):
    SELF_DECLARED = "self-declared"
    THIRD_PARTY = "third-party"

    @staticmethod
    def from_str(label):
        if label.lower() in ('self-declared', 'singleSelect'):
            return CertificationSchemeType.SELF_DECLARED
        elif label.lower() in ('certified', 'qualified'):
            return CertificationSchemeType.THIRD_PARTY
        else:
            raise NotImplementedError

class ComplianceReferenceGenerator:
        
    compliance_reference_map = {}
    """
        Static map to store CR.
        Key is CR-ID, value is a set {'type': self-declared|3-Party, '@id: 'did_web' }
    """
                         
    def __init__(self, template_compliance_reference, provider_base_did,
                 directory_for_compliance_reference, participant_hash,out_folder):
        self.template = utils.load_template(template_compliance_reference)
        self.provider_base_did = provider_base_did
        self.directory_for_compliance_reference = directory_for_compliance_reference
        self.participant_hash = participant_hash
        self.out_folder = out_folder
        self.filename = 'data.json'

    def build_compliance_reference_did_web(self, filename):
        return self.base_did_url_provider + self.base_compliance_reference_directory_for_did + filename

    def generate_reference(self, provider_did, csv_file_path, compliance_reference_manager: ComplianceReferenceManagerGenerator):
        output_directory = self.out_folder+'/'+self.participant_hash+self.directory_for_compliance_reference
        utils.clean_output_directory(output_directory)

        logging.debug("Generate Compliance references to %s",output_directory)
        
        with open(csv_file_path, newline='') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=";")
            for row in reader:
                print(row)
                title = row['Ref Designation']
                hash = hashlib.sha256(bytes(title,'utf-8')).hexdigest()
                did_web = self.provider_base_did + self.directory_for_compliance_reference + hash + '/'+self.filename
                output_filepath = os.path.join(output_directory, hash, self.filename)
                
                compliance_reference = self.to_json_ld(did_web, row, compliance_reference_manager.get_id())

                compliance_reference_manager.associate_compliance_reference(compliance_reference['@id'])
                if row['Assertion Method'] != '':
                    cert_scheme_type = CertificationSchemeType.from_str(row['Assertion Method'])
                    self.compliance_reference_map[row['CR-ID']] = {'type': cert_scheme_type, 'compliance_reference': copy.deepcopy(compliance_reference), 'filepath': output_filepath}
                    
        
                utils.save_on_disk_by_filepath(output_filepath, compliance_reference)
        
    def to_json_ld(self, id, row, compliance_reference_manager_id: str):
        data = self.template.copy()

        url = row['Reference Doc']
        title = row['Ref Designation']
        type = row['Category']
        version = row['Extrait']
        valid_from = utils.date_to_xds(row['Valid from']) if row['Valid from'] != '' else None
        valid_until = utils.date_to_xds(row['Valid Until']) if row['Valid Until'] != '' else None

        if url:
            sha256 = utils.computesha256(url)
            data['gax-compliance:hasReferenceUrl']["@value"] = url
            data['gax-compliance:hasSha256']["@value"] = sha256

        data['gax-compliance:referenceType'] = {'@value': type, "@type": "xsd:string"}
        data['gax-compliance:hasComplianceReferenceTitle']["@value"] = title
        data['gax-compliance:hasVersion'] = {"@value": version, "@type": "xsd:string"}

        if valid_from:
            data['gax-compliance:cRValidFrom'] = {"@value": valid_from, "@type": "xsd:dateTime"}

        if valid_until:
            data['gax-compliance:cRValidUntil'] = {"@value": valid_until, "@type": "xsd:dateTime"}

        data['gax-compliance:hasComplianceReferenceManager'] = {"@value": compliance_reference_manager_id,
                                                                "@type": "gax-participant:ComplianceReferenceManager"}
        data['@id'] = id

        return data

    def associate_certification_scheme(self, cr_id, scheme_web_id):    
        compliance_reference = self.compliance_reference_map[cr_id]['compliance_reference']
        filepath = self.compliance_reference_map[cr_id]['filepath']
        
        certification_schemes = compliance_reference.get("gax-compliance:hasComplianceCertificationSchemes", [])
        certification_schemes.append({"@id": scheme_web_id, "@type":"gax-compliance:ComplianceCertificationScheme"})
        
        compliance_reference["gax-compliance:hasComplianceCertificationSchemes"] =  certification_schemes
        utils.save_on_disk_by_filepath(filepath, compliance_reference)